module agayon.be/HTTPAuthentificationOverXMPP

go 1.17

require (
	agayon.be/http v0.0.0
	agayon.be/xmppauth v0.0.0

)

require (
	github.com/google/uuid v1.6.0 // indirect
	github.com/jimlawless/cfg v0.0.0-20160326141742-136e0c264d31
	golang.org/x/xerrors v0.0.0-20240903120638-7835f813f4da // indirect
	gosrc.io/xmpp v0.5.1 // indirect
	nhooyr.io/websocket v1.8.17 // indirect
)

replace (
	agayon.be/http => ./http
	agayon.be/xmppauth => ./xmppauth
	gosrc.io/xmpp => ./go-xmpp
)
