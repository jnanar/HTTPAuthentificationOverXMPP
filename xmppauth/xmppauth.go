package xmppauth

import (
	"fmt"

	"gosrc.io/xmpp"
	"gosrc.io/xmpp/stanza"

	"log"
	"strings"
)

const (
	LogInfo  = "\t[XMPP INFO]\t"
	LogError = "\t[XMPP ERROR]\t"
	LogDebug = "\t[XMPP DEBUG]\t"

	DEFAULT_SERVER_ADDRESS = "127.0.0.1"
	DEFAULT_SERVER_PORT    = "5347"
)

var (
	Addr               = ""
	Port               = ""
	CompJID            = ""
	Secret             = ""
	SoftVersion        = ""
	stanzaID           = 0
	Debug              = true
	VerifyCertValidity = true
	Comp               *xmpp.Component
	Stream             *xmpp.StreamManager
	ChanAction         = make(chan string)
	WaitMessageAnswers = make(map[string]*Confirmation)
	WaitIqMessages     = make(map[string]*Confirmation)
)

func Run() {

	log.Printf("%sRunning", LogInfo)
	addr := DEFAULT_SERVER_ADDRESS + ":" + DEFAULT_SERVER_PORT
	Addr = DEFAULT_SERVER_ADDRESS
	Port = DEFAULT_SERVER_PORT

	opts := xmpp.ComponentOptions{
		TransportConfiguration: xmpp.TransportConfiguration{
			Address: addr,
			Domain:  CompJID,
		},
		Domain:   CompJID,
		Secret:   Secret,
		Name:     "HTTP authentification component (XEP-070)",
		Category: "component",
		Type:     "generic",
	}

	router := xmpp.NewRouter()
	router.HandleFunc("message", handleMessage)
	router.NewRoute().
		IQNamespaces(stanza.NSDiscoInfo).
		HandlerFunc(func(s xmpp.Sender, p stanza.Packet) {
			discoInfo(s, p, &opts)
		})
	router.NewRoute().
		IQNamespaces(stanza.NSDiscoItems).
		HandlerFunc(func(s xmpp.Sender, p stanza.Packet) {
			discoInfo(s, p, &opts)
		})
	router.NewRoute().
		IQNamespaces("jabber:iq:version").
		HandlerFunc(handleVersion)
	router.NewRoute().
		IQNamespaces("http://jabber.org/protocol/http-auth").
		HandlerFunc(handleIQConfirm)

	// sessionID, stream_err := stanza.InitStream()
	var err error

	Comp, err = xmpp.NewComponent(opts, router, handleError)
	if err != nil {
		log.Fatalf("FATAL ERROR: %+v", err)
	}

	// If you pass the component to a stream manager, it will handle the reconnect policy
	// for you automatically.
	Stream = xmpp.NewStreamManager(Comp, nil)
	log.Printf("%sConnecting to %s", LogInfo, Addr)
	Comp.Connect()

}

func must(v interface{}, err error) interface{} {
	if err != nil {
		log.Fatal(LogError, err)
	}
	return v
}

func handleError(err error) {
	errorMesg := fmt.Sprintf("Fatal component Error: %s\n", err)
	log.Fatal(errorMesg)
}

func processConfirm(answer bool, confirmation *Confirmation) {
	if answer == false {
		log.Printf("%s%s refused to authenticate", LogInfo, confirmation.JID)
		confirmation.ChanReply <- REPLY_DENY
	} else {
		log.Printf("%s%s is authenticated", LogInfo, confirmation.JID)
		confirmation.ChanReply <- REPLY_OK
	}

}

func handleIQConfirm(s xmpp.Sender, p stanza.Packet) {
	answerIq, ok := p.(*stanza.IQ)
	if !ok {
		log.Printf("%sError with stanza %s", LogInfo, answerIq.Id)
		return
	}
	log.Printf("%sConfirmation IQ from %s", LogInfo, answerIq.From)
	bareFrom := strings.SplitN(answerIq.From, "/", 2)[0]
	confirmation := WaitIqMessages[answerIq.Id]
	var authValue bool
	if answerIq.Type == "error" {
		// We need to check the error to see which one it is
		if answerIq.Error.Reason == "service-unavailable" || answerIq.Error.Reason == "feature-not-implemented" {
			confirmation.JID = bareFrom
			log.Printf("%sAuthentication of %s by IQ is not supported", LogInfo, confirmation.JID)
			go confirmation.SendConfirmation()
			return
		}
		// user refuse to authenticate
		authValue = false
	}
	if answerIq.Type == "result" && confirmation.JID == answerIq.From {
		authValue = true
	}
	processConfirm(authValue, confirmation)

}

func handleMessage(_ xmpp.Sender, p stanza.Packet) {
	answer, ok := p.(stanza.Message)
	if !ok {
		return
	}
	bareFrom := strings.SplitN(answer.From, "/", 2)[0]
	// The answer MUST contain the original confirm
	for _, ext := range answer.Extensions {
		confirm, ok := ext.(*stanza.ConfirmPayload)
		if !ok {
			messageConfirmation := WaitMessageAnswers[answer.Body]
			if messageConfirmation != nil && messageConfirmation.JID == bareFrom {
				log.Printf("%sAuthentication of %s by Message Body", LogInfo, messageConfirmation.JID)
				processConfirm(true, messageConfirmation)
				return
			}
			continue
		}
		confirmation := WaitMessageAnswers[confirm.ID]
		if answer.Type != "error" && confirmation != nil && confirmation.JID == bareFrom {
			processConfirm(true, confirmation)
			return
		} else if answer.Type == "error" && answer.Error.Code == 401 && answer.Error.Type == "auth" {
			// The user refused explicitly to authenticate
			if answer.Error.Reason == "service-unavailable" {
				confirmation.ChanReply <- REPLY_UNREACHABLE
				continue
			}
			if confirmation != nil {
				log.Printf("%s%s explicitely refused to authenticate", LogInfo, confirmation.JID)
				processConfirm(false, confirmation)
				return
			}
		} else {
			log.Printf("%s%s could not authenticate: %s", LogInfo, confirmation.JID, answer.Type)
			processConfirm(false, confirmation)
			return
		}

	}
}

func discoInfo(c xmpp.Sender, p stanza.Packet, opts *xmpp.ComponentOptions) {
	// Type conversion & sanity checks
	iq, ok := p.(*stanza.IQ)
	if !ok || iq.Type != stanza.IQTypeGet {
		return
	}

	iqResp, err := stanza.NewIQ(stanza.Attrs{Type: "result", From: iq.To, To: iq.From, Id: iq.Id, Lang: "en"})
	// TODO: fix this...
	if err != nil {
		return
	}
	disco := iqResp.DiscoInfo()
	disco.AddIdentity(opts.Name, opts.Category, opts.Type)
	disco.AddFeatures(stanza.NSDiscoInfo, stanza.NSDiscoItems, "http://jabber.org/protocol/http-auth")
	_ = c.Send(iqResp)
}

func handleVersion(c xmpp.Sender, p stanza.Packet) {
	// Type conversion & sanity checks
	iq, ok := p.(*stanza.IQ)
	if !ok {
		return
	}

	iqResp, err := stanza.NewIQ(stanza.Attrs{Type: "result", From: iq.To, To: iq.From, Id: iq.Id, Lang: "en"})
	if err != nil {
		return
	}
	iqResp.Version().SetInfo("HTTP authentification component", "v0.6-dev", "")
	_ = c.Send(iqResp)
}
