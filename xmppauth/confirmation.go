package xmppauth

import (
	"gosrc.io/xmpp/stanza"

	"crypto/rand"
	"fmt"
	"io"
	"log"
	"strconv"
	"strings"
)

const (
	REPLY_UNREACHABLE = "reply_unreachable"
	REPLY_DENY        = "reply_deny"
	REPLY_OK          = "reply_ok"

	TYPE_SEND_MESSAGE = "type_send_message"
	TYPE_SEND_IQ      = "type_send_iq"

	TEMPLATE_DOMAIN          = "_DOMAIN_"
	TEMPLATE_METHOD          = "_METHOD_"
	TEMPLATE_VALIDATION_CODE = "_VALIDE_CODE_"
	DEFAULT_MESSAGE          = "_DOMAIN_ (with method _METHOD_) need to validate your identity, do you agree ?\nValidation code : _VALIDE_CODE_\nPlease check that this code is the same as on _DOMAIN_.\n\nIf your client doesn't support that functionnality, please send back the validation code to confirm the request."
)

var (
	MapLangs = make(map[string]string)
)

type MessageBody struct {
	Lang  string `xml:"xml:lang,attr,omitempty"`
	Value string `xml:body",chardata"`
}

type Confirmation struct {
	JID         string
	Method      string
	Domain      string
	Transaction string

	TypeSend string
	IdMap    string

	ChanReply chan string
}

func (confirmation *Confirmation) SendConfirmation() {
	log.Printf("%sRequest sent to %s", LogInfo, confirmation.JID)
	clientJID, _ := stanza.NewJid(confirmation.JID)
	if clientJID.Resource == "" {
		confirmation.askViaMessage()
	} else {
		confirmation.askViaIQ()
	}
}

func (confirmation *Confirmation) askViaIQ() {
	stanzaID++
	stanzaIDstr := strconv.Itoa(stanzaID)
	confirmIQ, _ := stanza.NewIQ(stanza.Attrs{Type: stanza.IQTypeGet, To: confirmation.JID,
		From: CompJID, Id: stanzaIDstr})

	confirmIQ.Confirm().SetConfirm(confirmation.Transaction, confirmation.Method, confirmation.Domain)
	Comp.Send(confirmIQ)
	WaitIqMessages[stanzaIDstr] = confirmation
	confirmation.TypeSend = TYPE_SEND_IQ
	confirmation.IdMap = stanzaIDstr
}

func (confirmation *Confirmation) askViaMessage() {
	message := stanza.NewMessage(stanza.Attrs{Type: "chat", To: confirmation.JID, From: CompJID})
	confirmation.setBodies(&message)
	message.Thread = RandomID()
	confirm := stanza.ConfirmPayload{
		ID:     confirmation.Transaction,
		Method: confirmation.Method,
		URL:    confirmation.Domain,
	}
	message.Extensions = append(message.Extensions, confirm)
	confirmation.TypeSend = TYPE_SEND_MESSAGE
	confirmation.IdMap = confirmation.Transaction
	// Send the message...
	Comp.Send(message)
	WaitMessageAnswers[confirmation.Transaction] = confirmation
}

func (confirmation *Confirmation) setBodies(message *stanza.Message) {
	msg := DEFAULT_MESSAGE
	if len(MapLangs) == 0 {
		msg = strings.Replace(msg, TEMPLATE_DOMAIN, confirmation.Domain, -1)
		msg = strings.Replace(msg, TEMPLATE_METHOD, confirmation.Method, -1)
		msg = strings.Replace(msg, TEMPLATE_VALIDATION_CODE, confirmation.Transaction, -1)
		message.Body = msg
	} else {
		// default english
		// go-xmpp do not support multiple bodies. We use HTML to provide several lang messages
		msg = strings.Replace(msg, TEMPLATE_DOMAIN, confirmation.Domain, -1)
		msg = strings.Replace(msg, TEMPLATE_METHOD, confirmation.Method, -1)
		msg = strings.Replace(msg, TEMPLATE_VALIDATION_CODE, confirmation.Transaction, -1)
		message.Body = msg
		for key, val := range MapLangs {
			msg = val
			msg = strings.Replace(msg, TEMPLATE_DOMAIN, confirmation.Domain, -1)
			msg = strings.Replace(msg, TEMPLATE_METHOD, confirmation.Method, -1)
			msg = strings.Replace(msg, TEMPLATE_VALIDATION_CODE, confirmation.Transaction, -1)
			msg = strings.Replace(msg, "\\n", "\n", -1)
			msg = strings.Replace(msg, "\\r", "\r", -1)
			msg = strings.Replace(msg, "\\t", "\t", -1)
			msg = fmt.Sprintf("<p>%s</p>", msg)
			html := stanza.HTML{Body: stanza.HTMLBody{InnerXML: msg}, Lang: key}
			message.Extensions = append(message.Extensions, html)
		}
	}
}

//--------------
// HELPERS
//--------------
// IDLen is the standard length of stanza identifiers in bytes.
const IDLen = 16

// RandomID generates a new random identifier of length IDLen. If the OS's
// entropy pool isn't initialized, or we can't generate random numbers for some
// other reason, panic.
func RandomID() string {
	return randomID(IDLen, rand.Reader)
}

// RandomLen is like RandomID but the length is configurable.
func RandomLen(n int) string {
	return randomID(n, rand.Reader)
}

func randomID(n int, r io.Reader) string {
	b := make([]byte, (n/2)+(n&1))
	switch n, err := r.Read(b); {
	case err != nil:
		panic(err)
	case n != len(b):
		panic("Could not read enough randomness")
	}
	return fmt.Sprintf("%x", b)[:n]
}
